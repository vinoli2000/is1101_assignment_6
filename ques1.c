#include <stdio.h>
void print(int num);
void pattern(int n, int i);

void pattern(int n, int i)
{
    if (n == 0)
        return;
    print(i);
    printf("\n");

    pattern(n - 1, i + 1);
}

void print(int num)
{
       if (num == 0)
        return;
    printf("%d", num);

    print(num - 1);
}
int main()
{
    int n;
    printf ("Enter the number:");
    if(scanf("%d", &n) != 1)
    {
        printf("Invalid number");
        return 1;
    }
    pattern(n, 1);
    return 0;
}
